#include <opencv2/highgui/highgui.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/imgproc/imgproc_c.h>
#include <cassert>
#include <iostream>

const char  * WINDOW_NAME  = "Face Tracker";
#define CASCADE_NAME_LEN 2048
char    EYE_CASCADE_NAME[CASCADE_NAME_LEN] = "../haarcascades/haarcascade_eye.xml";
char    FACE_CASCADE_NAME[CASCADE_NAME_LEN] = "../haarcascades/haarcascade_frontalface_alt2.xml";

using namespace std;

int main (int , char * const []) 
{
    const int scale = 1;

    // create all necessary instances
    cvNamedWindow (WINDOW_NAME, CV_WINDOW_AUTOSIZE);
    CvCapture * camera = cvCreateCameraCapture( CV_CAP_ANY );
    if (!camera)
    {
        cerr << "No camera available" << endl; // Si no se pudo abrir la cámara manda error.
        return -1;
    }
    //cvSetCaptureProperty(camera, CV_CAP_PROP_FRAME_WIDTH, 320);
    //cvSetCaptureProperty(camera, CV_CAP_PROP_FRAME_HEIGHT, 240);
    cvSetCaptureProperty(camera, CV_CAP_PROP_FPS, 15);
    
    cout << "Se abrió la cámara." << endl;
    
    CvHaarClassifierCascade* eye_cascade = (CvHaarClassifierCascade*) cvLoad (EYE_CASCADE_NAME, 0, 0, 0);
    CvHaarClassifierCascade* face_cascade = (CvHaarClassifierCascade*) cvLoad (FACE_CASCADE_NAME, 0, 0, 0);
    CvMemStorage* storage = cvCreateMemStorage(0);
    assert (storage);

    // did we load the cascade?!?
    if (! eye_cascade)
    {
        cerr << "No eye cascade file." << endl;
        return -1;
    }
    if (! face_cascade)
    {
        cerr << "No face cascade file." << endl;
        return -1;
    }

    // get an initial frame and duplicate it for later work
    IplImage *  current_frame = cvQueryFrame (camera);
    IplImage *  draw_image    = cvCreateImage(cvSize (current_frame->width, current_frame->height), IPL_DEPTH_8U, 3);
    IplImage *  gray_image    = cvCreateImage(cvSize (current_frame->width, current_frame->height), IPL_DEPTH_8U, 1);
    IplImage *  small_image   = cvCreateImage(cvSize (current_frame->width / scale, current_frame->height / scale), IPL_DEPTH_8U, 1);
    assert (small_image && gray_image && draw_image);
    
    int key;
    // as long as there are images ...
    do
    {
        cvShowImage (WINDOW_NAME, current_frame);
        cout << "Show image " << WINDOW_NAME << endl;
        // convert to gray and downsize
        cvCvtColor (current_frame, gray_image, CV_BGR2GRAY);
        cvResize (gray_image, small_image, CV_INTER_LINEAR);
        
        // detect faces
        CvSeq* eyes = cvHaarDetectObjects (small_image, eye_cascade, storage,
                                            1.1, 2, CV_HAAR_DO_CANNY_PRUNING,
                                            cvSize (30, 30));
        CvSeq* faces = cvHaarDetectObjects (small_image, face_cascade, storage,
                                            1.1, 2, CV_HAAR_DO_CANNY_PRUNING,
                                            cvSize (30, 30));
        // draw faces
        CvRect *face, *largestface;
        int largestfacesize = 0, facesize;
        for (int i = 0; i < (faces ? faces->total : 0); i++)
        {
            face = (CvRect*) cvGetSeqElem (faces, i);
            facesize = face->width + face->height;
            if (facesize > largestfacesize)
            {
                largestface     = face;
                largestfacesize = facesize;
            }
        }
        CvPoint face_center;
        int face_radius;
        face_center.x = cvRound((largestface->x + largestface->width*0.5) *scale);
        face_center.y = cvRound((largestface->y + largestface->height*0.5)*scale);
        face_radius = cvRound((largestface->width + largestface->height)*0.25*scale);
        //cvCircle (current_frame, face_center, face_radius, CV_RGB(255,0,0), 3, 8, 0 );
        cvRectangleR(current_frame, *largestface, CV_RGB(255,0,0), 3, 8, 0 );

        for (int i = (eyes ? eyes->total : 0); i >= 0 ; i--)
        {
            int radius;
            CvRect* r = (CvRect*) cvGetSeqElem (eyes, i);
            CvPoint center;
            center.x = cvRound((r->x + r->width*0.5) *scale);
            center.y = cvRound((r->y + r->height*0.5)*scale);
            radius = cvRound((r->width + r->height)*0.25*scale);
            
            if(center.y > face_center.y)
            {
                cvSeqRemove(eyes,i);
                cvCircle ( current_frame, center, radius, CV_RGB(255,255,255), 3, 8, 0 );
                break;
            }
            
            if ( 
                    (center.x < largestface->x) || (center.x > largestface->x+largestface->width ) ||
                    (center.y < largestface->y) || (center.y > largestface->y+largestface->height)
               )
            {
                cvSeqRemove(eyes,i);
                cvCircle ( current_frame, center, radius, CV_RGB(255,255,255), 3, 8, 0 );
                break;                
            }

            int face_center_x = cvRound((largestface->x + largestface->width*0.5) *scale);
            if (center.x > face_center_x)
            {
                cvRectangleR( current_frame, *r, CV_RGB(0,255,0), 3, 8, 0 );
                cvCircle( current_frame,center,2,CV_RGB(0,255,0), 3, 8, 0 );
            }
            if (center.x <= face_center_x)
            {
                cvRectangleR( current_frame, *r, CV_RGB(0,0,255), 3, 8, 0 );
                cvCircle( current_frame,center,2,CV_RGB(0,0,255), 3, 8, 0 );
            }
        }

        cout << "eyes " << (int)(eyes ? eyes->total : 0) << endl;
        cout << "faces " << (int)(faces ? faces->total : 0) << endl;

        cvFlip (current_frame, draw_image, 1);
        
        // just show the image
        cvShowImage (WINDOW_NAME, draw_image);
        
        // wait for keypress
        key = cvWaitKey (33);
        current_frame = cvQueryFrame (camera);
    } while (key != 'q' && key != 'Q');
    
    cvReleaseCapture( &camera );
    cvReleaseImage( &draw_image );
    cvReleaseImage( &gray_image );
    cvReleaseImage( &small_image );
    
    cvDestroyWindow (WINDOW_NAME);
    
    // be nice and return no error
    return 0;
}
